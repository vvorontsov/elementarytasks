package com.softserve.edu.task6;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.nio.charset.StandardCharsets.UTF_8;

class Ticket {
    private static Integer count = 0;
    private final Integer number = ++count;
    private boolean isItLucky = false;
    private ArrayList<Integer> valueOfNumber = new ArrayList<Integer>();

    public ArrayList<Integer> getNumber() {
        return valueOfNumber;
    }

    public void setNumber(ArrayList<Integer> number) {
        this.valueOfNumber = number;
    }

    public void setLucky() {
        isItLucky = true;
    }

    public void setUnlucky() {
        isItLucky = false;
    }

    public boolean itsLuckyTicket(){
        return isItLucky;
    }

    @Override
    public String toString() {
        String valueNumber = "";
        for (Integer integer : valueOfNumber) {
            valueNumber += integer;
        }
        String str = "Ticket #" + number + " " + "Number: " + valueNumber + "; ";
        if (isItLucky) {
            str += "Lucky";
        } else {
            str += "Unlucky";
        }
        return str;
    }
}

public class App {
    private static ArrayList<Ticket> listOfTickets = new ArrayList<Ticket>();
    private static String kindOfChecking;

    public static ArrayList<Integer> stringToArrayOfInteger(String string)
            throws ClassCastException {
        ArrayList<Integer> returnList = new ArrayList<Integer>();
        char[] myArray = string.toCharArray();
        for (int i = 0; i < myArray.length; i++) {
            if (Character.digit(myArray[i], 10) == -1) {
                throw new ClassCastException();
            }
            returnList.add(Character.digit(myArray[i], 10));
        }
        return returnList;
    }

    private static String enterTheWayToFile() {
        System.out.print("Enter the way to file: ");
        Scanner in = new Scanner(System.in);
        if (!in.hasNextLine()) {
            System.out.println("You need to enter the way to the txt file");
            enterTheWayToFile();
        }
        try {
            return in.nextLine();
        } finally {
            in.close();
        }
    }

    private static ArrayList<Ticket> makeListFromFile() {
        String wayToFile = enterTheWayToFile();
        ArrayList<Ticket> returnList = new ArrayList<Ticket>();
        List<String> lines = new ArrayList<String>();
        try {
            lines = Files.readAllLines(Paths.get(wayToFile), UTF_8);
        } catch (IOException e) {
            System.out.println("There is no such file, try again");
            return makeListFromFile();
        }
        for (String str : lines) {
            if (lines.indexOf(str) == 0) {
                str = str.substring(1);
                kindOfChecking = str.toLowerCase();
                if (!kindOfChecking.equals("moskow") &&
                        !kindOfChecking.equals("piter")) {
                    System.out.println("Illegal kind of cheking!");
                    return makeListFromFile();
                }
                continue;
            }
            ArrayList<Integer> list = new ArrayList<Integer>();
            try {
                list = stringToArrayOfInteger(str);
                if (list.size() == 6) {
                    Ticket tick = new Ticket();
                    tick.setNumber(list);
                    returnList.add(tick);
                } else {
                    System.out.println("Invalid type of input: " + str);
                }
            } catch (ClassCastException e) {
                System.out.println("Invalid type of input: " + str);
            }
        }
        return returnList;
    }

    private static void moskowCheck(Ticket ticket) {
        Integer int1 = ticket.getNumber().get(0) + ticket.getNumber().get(1)
                + ticket.getNumber().get(2);
        Integer int2 = ticket.getNumber().get(3) + ticket.getNumber().get(4)
                + ticket.getNumber().get(5);
        if (int1.equals(int2)) {
            ticket.setLucky();
        }
    }

    private static void piterCheck(Ticket ticket) {
        ArrayList<Integer> chetn = new ArrayList<Integer>();
        ArrayList<Integer> nechetn = new ArrayList<Integer>();
        for (Integer integer: ticket.getNumber()) {
            if (integer % 2 == 0) {
                chetn.add(integer);
            } else {
                nechetn.add(integer);
            }
        }
        Integer int1 = 0;
        Integer int2 = 0;
        for (Integer integer: chetn) {
            int1 += integer;
        }
        for (Integer integer: nechetn) {
            int2 += integer;
        }
        if (int1.equals(int2)) {
            ticket.setLucky();
        }
    }

    public static void checkTheTicket(ArrayList<Ticket> listOfTick) {
        for (Ticket ticket: listOfTick) {
            if (kindOfChecking.equals("moskow")) {
                moskowCheck(ticket);
                continue;
            }
            if (kindOfChecking.equals("piter")) {
                piterCheck(ticket);
            }
        }
    }

    public static void printThiListOfTickets(ArrayList<Ticket> ticketsList) {
        System.out.println("\nList of the Tickets \nKind Of check: "
                + kindOfChecking);
        Integer count = 0;
        for (Ticket ticket: ticketsList) {
            System.out.println(ticket);
            if (ticket.itsLuckyTicket()) {
                count++;
            }
        }
        System.out.println("Total count of lucky tickets: " + count);
    }

    public static void main(String[] args) {
        listOfTickets = makeListFromFile();
        checkTheTicket(listOfTickets);
        printThiListOfTickets(listOfTickets);
    }
}
