package com.softserve.edu.task5;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class App {

    private static LinkedList<ArrayList<String>> beforeThousand =
            new LinkedList<ArrayList<String>>();
    private static ArrayList<String> teens =
            new ArrayList<String>();
    private static LinkedList<ArrayList<String>> edinitsy =
            new LinkedList<ArrayList<String>>();
    private static LinkedList<ArrayList<String>> namesOfNumbers =
            new LinkedList<ArrayList<String>>();

    static {
        ArrayList<String> array1 = new ArrayList<String>();
        array1.add("сто");
        array1.add("двести");
        array1.add("триста");
        array1.add("четыреста");
        array1.add("пятьсот");
        array1.add("шестьсот");
        array1.add("семьсот");
        array1.add("восемьсот");
        array1.add("девятьсот");
        beforeThousand.add((ArrayList<String>) array1.clone());
        array1.clear();
        array1.add("десять");
        array1.add("двадцать");
        array1.add("тридцать");
        array1.add("сорок");
        array1.add("пятьдесят");
        array1.add("шестьдесят");
        array1.add("семьдесят");
        array1.add("восемьдесят");
        array1.add("девяносто");
        beforeThousand.add((ArrayList<String>) array1.clone());
        array1.clear();
        teens.add("десять");
        teens.add("одиннадцать");
        teens.add("двенадцать");
        teens.add("тринадцать");
        teens.add("четырнадцать");
        teens.add("пятнадцать");
        teens.add("шестнадцать");
        teens.add("семнадцать");
        teens.add("восемнадцать");
        teens.add("девятнадцать");
        array1.add("одна");
        array1.add("две");
        array1.add("три");
        array1.add("четыре");
        array1.add("пять");
        array1.add("шесть");
        array1.add("семь");
        array1.add("восемь");
        array1.add("девять");
        array1.add("десять");
        edinitsy.add((ArrayList<String>) array1.clone());
        array1.clear();
        array1.add("один");
        array1.add("два");
        array1.add("три");
        array1.add("четыре");
        array1.add("пять");
        array1.add("шесть");
        array1.add("семь");
        array1.add("восемь");
        array1.add("девять");
        array1.add("десять");
        edinitsy.add((ArrayList<String>) array1.clone());
        array1.clear();
        array1.add("");
        array1.add("");
        array1.add("");
        array1.add("");
        array1.add("");
        array1.add("");
        array1.add("");
        array1.add("");
        array1.add("");
        array1.add("");
        namesOfNumbers.add((ArrayList<String>) array1.clone());
        array1.clear();
        array1.add("тысяч");
        array1.add("тысяча");
        array1.add("тысячи");
        array1.add("тысячи");
        array1.add("тысячи");
        array1.add("тысяч");
        array1.add("тысяч");
        array1.add("тысяч");
        array1.add("тысяч");
        array1.add("тысяч");
        namesOfNumbers.add((ArrayList<String>) array1.clone());
        array1.clear();
        array1.add("миллионов");
        array1.add("миллион");
        array1.add("миллиона");
        array1.add("миллиона");
        array1.add("миллиона");
        array1.add("миллионов");
        array1.add("миллионов");
        array1.add("миллионов");
        array1.add("миллионов");
        array1.add("миллионов");
        namesOfNumbers.add((ArrayList<String>) array1.clone());
        array1.clear();
        array1.add("миллиардов");
        array1.add("миллиард");
        array1.add("миллиарда");
        array1.add("миллиарда");
        array1.add("миллиарда");
        array1.add("миллиардов");
        array1.add("миллиардов");
        array1.add("миллиардов");
        array1.add("миллиардов");
        array1.add("миллиардов");
        array1.add("миллиардов");
        namesOfNumbers.add((ArrayList<String>) array1.clone());
        array1.clear();
        array1.clear();
        array1.add("триллионов");
        array1.add("триллион");
        array1.add("триллиона");
        array1.add("триллиона");
        array1.add("триллиона");
        array1.add("триллионов");
        array1.add("триллионов");
        array1.add("триллионов");
        array1.add("триллионов");
        array1.add("триллионов");
        namesOfNumbers.add((ArrayList<String>) array1.clone());
        array1.clear();
        array1.add("квадрильонов");
        array1.add("квадрильон");
        array1.add("квадрильона");
        array1.add("квадрильона");
        array1.add("квадрильона");
        array1.add("квадрильонов");
        array1.add("квадрильонов");
        array1.add("квадрильонов");
        array1.add("квадрильонов");
        array1.add("квадрильонов");
        namesOfNumbers.add((ArrayList<String>) array1.clone());
        array1.clear();
    }

    public static ArrayList<Integer> stringToArrayOfInteger(String string)
            throws ClassCastException {
        ArrayList<Integer> returnList = new ArrayList<Integer>();
        char[] myArray = string.toCharArray();
        for (int i = 0; i < myArray.length; i++) {
            if (Character.digit(myArray[i], 10) == -1) {
                throw new ClassCastException();
            }
            returnList.add(Character.digit(myArray[i], 10));
        }
        return returnList;
    }

    public static String makeTextFromNumber(String string) {
        ArrayList<Integer> listOfIntegers = new ArrayList<Integer>();
        try {
            listOfIntegers = stringToArrayOfInteger(string);
        } catch (ClassCastException e) {
            return "Invalid input";
        }
        if (listOfIntegers.size() >= 1) {
            for (int i = (listOfIntegers.size() - 1); i >= 0; i = i - 3) {
                if ((i - 2) >= 0) {
                    listOfIntegers.set(i - 1, listOfIntegers.get(i - 2) * 10 +
                            listOfIntegers.get(i - 1));
                    listOfIntegers.set(i - 2, 100);
                }
            }
        }
        for (int i = 0; i < listOfIntegers.size(); i++) {
            if (listOfIntegers.get(i).equals(100)) {
                listOfIntegers.remove(i);
            }
        }
        Integer index = 0;
        String returnStat = "";
        for (int i = listOfIntegers.size() - 1; i >= 0; i = i - 2) {
            if ((i - 1) >= 0) {
                returnStat = makeTextFromPair(listOfIntegers.get(i - 1),
                        listOfIntegers.get(i), index) + returnStat;
            } else {
                returnStat = makeTextFromPair(0, listOfIntegers.get(i),
                        index) + returnStat;
            }
            index++;
        }
        return returnStat;
    }


    public static String makeTextFromPair(Integer int1, Integer int2,
                                          Integer index) {
        String returnStat = "";
        if (int1.equals(0) && int2.equals(0)) {
            return returnStat;
        }
        //Если в разряде присутствуют сотни и\или десятки
        if (!int1.equals(0)) {
            //Если есть сотни
            if (int1 >= 10) {
                //Узнаём количество сотен и десятков
                Integer int1_1 = (Integer) (int1 / 10);
                Integer int1_2 = int1 - int1_1 * 10;
                //Записываем соответсвующее название сотен
                returnStat = beforeThousand.get(0).get(int1_1 - 1) + " ";
                //Если десятки не нулевые
                if (!int1_2.equals(0)) {
                    //Проверяем, находятся ли десятки в диапазоне (10-19),
                    //так как для этого диапазона уникальны
                    //имена десятков
                    if (int1_2.equals(1)) {
                        returnStat += teens.get(int2) + " ";
                        returnStat += namesOfNumbers.get(index).get(0) + " ";
                        //Если десятки вне диапазона (10-19)
                    } else {
                        returnStat += beforeThousand.get(1).get(int1_2 - 1) + " ";
                        returnStat += makeEdinitsy(int2, index);
                    }
                    //Если десятки нулевые
                } else {
                    //И не нулевые единицы
                    if (!int2.equals(0)) {
                        returnStat += makeEdinitsy(int2, index);
                    } else {
                        //Иначе задаём название разряда прямо после сотен
                        returnStat += namesOfNumbers.get(index).get(0) + " ";
                    }
                }
                //Если сотен нет
            } else {
                if (int1.equals(1)) {
                    returnStat += teens.get(int2) + " ";
                    returnStat += namesOfNumbers.get(index).get(0) + " ";
                } else {
                    returnStat += beforeThousand.get(1).get(int1 - 1) + " ";
                    returnStat += makeEdinitsy(int2, index);
                }
            }
            //Если нет ни сотен, ни десятков, переходим сразу к единицам
        } else {
            if (!int2.equals(0)) {
                returnStat += makeEdinitsy(int2, index);
            }
        }
        return returnStat;
    }

    private static String makeEdinitsy(Integer int2, Integer index) {
        String returnStat = "";
        if (index.equals(1)) {
            returnStat += edinitsy.get(0).get(int2 - 1) + " ";
            returnStat += namesOfNumbers.get(1).get(int2) + " ";
        } else {
            returnStat += edinitsy.get(1).get(int2 - 1) + " ";
            returnStat += namesOfNumbers.get(index).get(int2) + " ";
        }
        return returnStat;
    }

    public static String enterTheValue() {
        System.out.print("Введите значение: ");
        Scanner in = new Scanner(System.in);
        if (!in.hasNextLine()) {
            System.out.println("Введено пустое значение, введите ещё раз");
            enterTheValue();
        }
        try {
            return in.nextLine();
        } finally {
            in.close();
        }
    }

    public static void main(String[] args) {
        String str = enterTheValue();
        System.out.println(makeTextFromNumber(str));
    }
}
