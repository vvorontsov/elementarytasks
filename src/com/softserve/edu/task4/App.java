package com.softserve.edu.task4;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.nio.charset.StandardCharsets.UTF_8;

public class App {
    private static ArrayList<String> enterTheValues() {
        ArrayList<String> returnList = new ArrayList<String>();
        System.out.print("Enter the values through the space " +
                "(please, use \"_\" in the value of string): ");
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        String[] massStr = str.split(" ");
        if (massStr.length < 2) {
            System.out.println("To few arguments");
            return enterTheValues();
        }
        if (massStr.length > 3) {
            System.out.println("To many arguments");
            return enterTheValues();
        }
        for (String st: massStr) {
            returnList.add(st);
        }
        for (int i=1; i<returnList.size(); i++) {
            returnList.set(i, returnList.get(i).replace('_', ' '));
        }
        in.close();
        return returnList;
    }

    private static List<String> makeListFromFile(String str) throws FileNotFoundException {
        List<String> lines = new ArrayList<String>();
        try {
            lines = Files.readAllLines(Paths.get(str), UTF_8);
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
        lines.set(0, lines.get(0).substring(1));
        return lines;
    }

    private static void kindOfPasce1(List<String> list, String compareStr) {
        Integer count = 0;
        for (String str : list) {
            if (str.equals(compareStr)) {
                count++;
            }
        }
        System.out.println("Total count of string \"" + compareStr + "\" is " + count);
    }

    private static void kindOfParce2(List<String> list, ArrayList<String> values) {
        String res = "";
        for (int i=0; i<list.size(); i++) {
            if (list.get(i).equals(values.get(1))) {
                list.set(i, values.get(2));
            }
            res += list.get(i) + "\n";
        }
        Path  path = Paths.get(values.get(0));
        System.out.println(res);
        try {
            Files.write(path, res.getBytes(java.nio.charset.StandardCharsets.UTF_8));
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void fileParse() {
        List<String> list = new ArrayList<String>();
        ArrayList<String> enteredValues = enterTheValues();
        try {
            list = makeListFromFile(enteredValues.get(0));
        } catch (FileNotFoundException e) {
            System.out.println("There is no such file, try again");
            fileParse();
            return;
        }
        if (enteredValues.size() == 2) {
            kindOfPasce1(list, enteredValues.get(1));
        }
        if (enteredValues.size()==3) {
            kindOfParce2(list, enteredValues);
        }
    }

    public static void main(String[] args) {
        fileParse();
    }
}
